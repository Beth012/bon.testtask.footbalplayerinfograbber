﻿function sendDump() {
    showLoader();

    var dump = $("#dump").val();
    var key = $("#key").val();
    var format = $("#format").val();
    $.ajax({
        type: 'POST',
        url: "api/Decryptor",
        data: JSON.stringify({
            dump: dump,
            key: key,
            format: format
        }),
        success: (rest) => {
            var superString = JSON.stringify(rest);
            $("#result").val(superString);
            $("#decryptedDumps").val(superString);
            hideLoader();
        },
        contentType: "application/json; charset=utf-8",

        error: (err) => {
            alert(err.status + "\n" + err.statusText);
            hideLoader();
        }
    });
}

function CheckDumpUniqueness() {
    showLoader();
    var client = $("#client").val();
    var username = $("#username").val();
    var token = $("#token").val();
    var result = $("#decryptedDumps").val();
    $.ajax({
        type: 'POST',
        url: "api/BugTracking",
        data: JSON.stringify({
            project: client,
            username: username,
            password: token,
            decryptedDump: JSON.parse(result)
        }),
        success: (rest) => {
            DisplayIssues(rest);
        },
        contentType: "application/json; charset=utf-8",

        error: (err) => {
            alert(err.status + "\n" + err.statusText);
            hideLoader();
        }
    });
}

    return (innerDiv + "</td>")
}