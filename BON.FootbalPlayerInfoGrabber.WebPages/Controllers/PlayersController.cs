﻿using System.Collections.Generic;
using System.Linq;
using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using BON.FootbalPlayerInfoGrabber.WebPages.Abstract;
using BON.FootbalPlayerInfoGrabber.WebPages.Models;
using Microsoft.AspNetCore.Mvc;

namespace BON.FootbalPlayerInfoGrabber.WebPages.Controllers
{
    public class PlayersController : Controller
    {
        private readonly IProviderPlayersInfoProviderService _playerGrabberInfoServiceInstance;
        private readonly IFootballPlayerPresentationMapper _footballPlayerPresentationMapperInstance;

        public PlayersController(IProviderPlayersInfoProviderService playerGrabberInfoServiceInstance, 
            IFootballPlayerPresentationMapper footballPlayerPresentationMapperInstance)
        {
            _playerGrabberInfoServiceInstance = playerGrabberInfoServiceInstance;
            _footballPlayerPresentationMapperInstance = footballPlayerPresentationMapperInstance;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new List<FootballPlayerPresentationModel>());
        }

        [HttpPost]
        public IActionResult GetPlayerProviderPlayers([FromForm] string providerUrl)
        {
            var players = _playerGrabberInfoServiceInstance.GetProviderPlayersAsync(providerUrl).Result
                .Select(_footballPlayerPresentationMapperInstance.ToFootballPlayerPresentationModel).ToList();

            return View("~/Views/Players/Index.cshtml", players);
        }
    }
}