﻿using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.WebPages.Components.GetProviderPlayers
{
    public class GetProviderPlayers : ViewComponent
    {
        private IProviderPlayersInfoProviderService _playerGrabberInfoServiceInstance;

        public GetProviderPlayers(IProviderPlayersInfoProviderService playerGrabberInfoServiceInstance)
        {
            _playerGrabberInfoServiceInstance = playerGrabberInfoServiceInstance;
        }
        public IViewComponentResult Invoke(string url)
        {          
            return View("_PlayersTable", _playerGrabberInfoServiceInstance.GetProviderPlayersAsync(url));
        }
    }
}
