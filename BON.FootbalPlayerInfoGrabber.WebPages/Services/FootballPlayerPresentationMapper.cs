﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.WebPages.Abstract;
using BON.FootbalPlayerInfoGrabber.WebPages.Models;
using System;

namespace BON.FootbalPlayerInfoGrabber.WebPages.Services
{
    /// <inheritdoc/>
    public class FootballPlayerPresentationMapper : IFootballPlayerPresentationMapper
    {
        public FootballPlayerPresentationModel ToFootballPlayerPresentationModel(IPlayer player)
        {
            if (player == null)
                throw new InvalidOperationException(nameof(player));

            FootballPlayerPresentationModel footballPlayerPresentationModel = new FootballPlayerPresentationModel
            {
                Name = player.Name,
                ImageUrl = player.ImageUrl,
                Games = player.Games
            };

            if (player is IFootballPlayer footballPlayer)
                footballPlayerPresentationModel.Goals = footballPlayer.Goals;

            return footballPlayerPresentationModel;
        }
    }
}
