using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using BON.FootbalPlayerInfoGrabber.DAL.Context;
using BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Services;
using BON.FootbalPlayerInfoGrabber.Services.Abstract;
using BON.FootbalPlayerInfoGrabber.Services.BackgroundServices;
using BON.FootbalPlayerInfoGrabber.Services.Services;
using BON.FootbalPlayerInfoGrabber.WebPages.Abstract;
using BON.FootbalPlayerInfoGrabber.WebPages.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BON.FootbalPlayerInfoGrabber.WebPages
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SportDBContext>(options =>
                        options.UseNpgsql(Configuration.GetConnectionString("DB_CONNECTION_STRING")));

            services.AddHostedService<DeleteExpiredPlayerProvidersInfoService>();

            services.AddSingleton<IFootballPlayerMapper, FootballPlayerMapper>();
            services.AddSingleton<IPlayerProviderMapper, PlayerProviderMapper>();
            services.AddSingleton<IFootballPlayerPresentationMapper, FootballPlayerPresentationMapper>();

            services.AddScoped<IPlayerCrudService, PlayerService>();
            services.AddScoped<IPlayerProviderCrudService, PlayerProvideService>();
            services.AddScoped<IPlayerInfoGrabberService, ExpressRuPlayerProvider>();
            services.AddScoped<IProviderPlayersInfoProviderService, PlayerGrabberInfoService>();
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Players}/{action=Index}");
            });

            using (var scope = app.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<SportDBContext>();
                context.Database.Migrate();
            }
        }
    }
}
