﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.WebPages.Models;

namespace BON.FootbalPlayerInfoGrabber.WebPages.Abstract
{
    /// <summary>
    /// Football player presentation mapper interface.
    /// </summary>
    public interface IFootballPlayerPresentationMapper
    {
        /// <summary>
        /// Maps football player model to football player presentation model.
        /// </summary>
        /// <param name="footballPlayer">The football player model.</param>
        /// <returns>The football player presentation model.</returns>
        FootballPlayerPresentationModel ToFootballPlayerPresentationModel(IPlayer footballPlayer);
    }
}
