﻿namespace BON.FootbalPlayerInfoGrabber.WebPages.Models
{
    /// <summary>
    /// Represents football player data.
    /// </summary>
    public class FootballPlayerPresentationModel
    {
        /// <summary>
        /// Gets or sets player image url.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets player games number.
        /// </summary>
        public int Games { get; set; }

        /// <summary>
        /// Gets or sets football player goals number.
        /// </summary>
        public int Goals { get; set; }
    }
}
