﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.DAL.Entities;
using BON.FootbalPlayerInfoGrabber.Services.Models;

namespace BON.FootbalPlayerInfoGrabber.Services.Abstract
{
    /// <summary>
    /// Football player mapper interface.
    /// </summary>
    public interface IFootballPlayerMapper
    {
        /// <summary>
        /// Maps football player model to football player entity model.
        /// </summary>
        /// <param name="footballPlayerModel">The football player entity model.</param>
        /// <returns>The football player entity model.</returns>
        FootballPlayer ToFootballPlayer(IFootballPlayer footballPlayerModel);

        /// <summary>
        /// Maps football player entity model to football player model.
        /// </summary>
        /// <param name="footballPlayer">The football player entity model.</param>
        /// <returns>The football player entity model.</returns>
        FootballPlayerModel ToFootballPlayerModel(FootballPlayer footballPlayer);
    }
}
