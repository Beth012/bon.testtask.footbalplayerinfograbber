﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using BON.FootbalPlayerInfoGrabber.DAL.Context;
using BON.FootbalPlayerInfoGrabber.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.Services.Services
{
    /// <inheritdoc/>
    public class PlayerService : BaseService, IPlayerCrudService
    {
        #region Fields

        private readonly IFootballPlayerMapper _footballPlayerMapper;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize new instance <see cref="PlayerService"/.>.
        /// </summary>
        /// <param name="sportDBContext">SportDBContext instance.</param>
        /// <param name="footballPlayerMapper">IFootballPlayerMapper instance.</param>
        public PlayerService(SportDBContext sportDBContext, IFootballPlayerMapper footballPlayerMapper) : base(sportDBContext)
        {
            _footballPlayerMapper = footballPlayerMapper;
        }

        #endregion

        #region IPlayerCrudService

        public async Task CreateRangeAsync(IEnumerable<IPlayer> players)
        {
            if (players == null)
                throw new InvalidOperationException(nameof(players));

            if (players is IEnumerable<IFootballPlayer> footballPlayers)
            {
                await DbContext.FootballPlayers.AddRangeAsync(footballPlayers.Select(_footballPlayerMapper.ToFootballPlayer));
                await DbContext.SaveChangesAsync();
            }
        }

        public IEnumerable<IPlayer> Get(string providerUrl)
        {
            return DbContext.FootballPlayers.Where(p => p.PlayerProvider.ProviderUrl == providerUrl).Select(_footballPlayerMapper.ToFootballPlayerModel).ToList();
        }

        #endregion
    }
}
