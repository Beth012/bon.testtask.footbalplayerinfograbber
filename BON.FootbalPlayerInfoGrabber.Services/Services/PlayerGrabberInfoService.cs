﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Services
{
    /// <inheritdoc/>
    public class PlayerGrabberInfoService : IProviderPlayersInfoProviderService
    {
        #region Fields

        private readonly IPlayerProviderCrudService _playerProviderCrudInstance;
        private readonly IPlayerCrudService _playerCrudInstance;
        private readonly IPlayerInfoGrabberService _playerInfoProviderInstance;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize new instance <see cref="PlayerGrabberInfoService"/.>.
        /// </summary>
        /// <param name="playerProviderCrudInstance">IPlayerProviderCrudService instance.</param>
        /// <param name="playerCrudInstance">IPlayerCrudService instance.</param>
        /// <param name="playerInfoProviderInstance">IPlayerInfoGrabberService instance.</param>
        public PlayerGrabberInfoService (IPlayerProviderCrudService playerProviderCrudInstance, 
            IPlayerCrudService playerCrudInstance, IPlayerInfoGrabberService playerInfoProviderInstance)
        {
            _playerProviderCrudInstance = playerProviderCrudInstance;
            _playerCrudInstance = playerCrudInstance;
            _playerInfoProviderInstance = playerInfoProviderInstance;
        }

        #endregion

        #region IProviderPlayersInfoProviderService

        public async Task<IEnumerable<IPlayer>> GetProviderPlayersAsync(string providerUrl)
        {
            ValidateUrl(providerUrl);
            await ProviderPlayerInfoExistanseHadler(providerUrl);

            return _playerCrudInstance.Get(providerUrl);
        }

        #endregion

        #region Private Methods

        private void ValidateUrl(string providerUrl)
        {
            if (!Uri.IsWellFormedUriString(providerUrl, UriKind.Absolute))
                throw new InvalidOperationException(nameof(providerUrl));           
        }

        private async Task ProviderPlayerInfoExistanseHadler(string providerUrl)
        {
            if (!IsPlayerProviderExists(providerUrl))
                await AddNewProviderInfoAsync(providerUrl);
        }
      
        private bool IsPlayerProviderExists(string providerUrl)
        {
            return _playerProviderCrudInstance.Get(providerUrl) != null;
        }

        private async Task AddNewProviderInfoAsync(string providerUrl)
        {
            await _playerProviderCrudInstance.CreateAsync(providerUrl);
            await CreatePlayerProviderPlayers(providerUrl);
        }

        private async Task CreatePlayerProviderPlayers(string providerUrl)
        {
            var players = _playerInfoProviderInstance.GrabPlayerInfo(providerUrl);
            await _playerCrudInstance.CreateRangeAsync(players);
        }

        #endregion
    }
}
