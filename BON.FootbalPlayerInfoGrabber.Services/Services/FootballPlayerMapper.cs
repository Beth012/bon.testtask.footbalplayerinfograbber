﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.DAL.Entities;
using BON.FootbalPlayerInfoGrabber.Services.Abstract;
using BON.FootbalPlayerInfoGrabber.Services.Models;
using System;

namespace BON.FootbalPlayerInfoGrabber.Services.Services
{
    /// <inheritdoc/>
    public class FootballPlayerMapper : IFootballPlayerMapper
    {
        #region Fields

        private readonly IPlayerProviderMapper _playerProviderMapperInstance;

        #endregion

        #region Construcor

        /// <summary>
        /// Initialize new instance <see cref="FootballPlayerMapper"/.>.
        /// </summary>
        /// <param name="playerProviderMapperInstance">IPlayerProviderMapper instance.</param>
        public FootballPlayerMapper(IPlayerProviderMapper playerProviderMapperInstance)
        {
            _playerProviderMapperInstance = playerProviderMapperInstance;
        }

        #endregion

        #region IFootballPlayerMapper

        public FootballPlayer ToFootballPlayer (IFootballPlayer footballPlayer)
        {
            if (footballPlayer == null)
                throw new InvalidOperationException(nameof(footballPlayer));

            FootballPlayer player = new FootballPlayer
            {
                Name = footballPlayer.Name,
                ImageUrl = footballPlayer.ImageUrl,
                Games = footballPlayer.Games,
                Goals = footballPlayer.Goals,
                PlayerProvider = _playerProviderMapperInstance.ToPlayerProvider(footballPlayer.PlayerProvider)
            };

            if (footballPlayer is FootballPlayerModel footballPlayerModel)
            {
                player.Id = footballPlayerModel.Id;
            }

            return player;
        }

        public FootballPlayerModel ToFootballPlayerModel(FootballPlayer footballPlayer)
        {
            if (footballPlayer == null)
                throw new InvalidOperationException(nameof(footballPlayer));

            return new FootballPlayerModel
            {
                Id = footballPlayer.Id,
                Name = footballPlayer.Name,
                ImageUrl = footballPlayer.ImageUrl,
                Games = footballPlayer.Games,
                Goals = footballPlayer.Goals,
                PlayerProvider = _playerProviderMapperInstance.ToPlayerProviderModel(footballPlayer.PlayerProvider)
            };
        }

        #endregion
    }
}
