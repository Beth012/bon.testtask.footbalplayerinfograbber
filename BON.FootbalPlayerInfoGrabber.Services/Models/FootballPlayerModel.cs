﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;

namespace BON.FootbalPlayerInfoGrabber.Services.Models
{
    /// <summary>
    /// Represents football player data.
    /// </summary>
    public class FootballPlayerModel : IFootballPlayer
    {
        /// <summary>
        /// Gets or sets player identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets player provider data.
        /// </summary>
        public IPlayerProvider PlayerProvider { get; set; }

        /// <summary>
        /// Gets or sets player image url.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets player games number.
        /// </summary>
        public int Games { get; set; }

        /// <summary>
        /// Gets or sets football player goals number.
        /// </summary>
        public int Goals { get; set; }
    }
}