﻿using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.Services.BackgroundServices
{
    /// <summary>
    /// Host service that contains API to start timer for clearing old device logs.
    /// </summary>
    public class DeleteExpiredPlayerProvidersInfoService : IHostedService
    {
        #region Fields

        private Timer _timer;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<DeleteExpiredPlayerProvidersInfoService> _logger;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize new instance <see cref="DeleteExpiredPlayerProvidersInfoService"/.>.
        /// </summary>
        /// <param name="serviceScopeFactory">Service scope factory.</param>
        /// <param name="logger">ILogger.</param>
        public DeleteExpiredPlayerProvidersInfoService(IServiceScopeFactory serviceScopeFactory,
            ILogger<DeleteExpiredPlayerProvidersInfoService> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }

        #endregion

        #region IHostedService

        /// <summary>
        /// Starts service.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Successfully completed task.</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(UpdatePlayerInfoProvider, null, TimeSpan.Zero,
                TimeSpan.FromHours(9));
            _logger.LogDebug("LogDebug");
            _logger.LogCritical("LogCritical");
            _logger.LogError("LogError");
            return Task.CompletedTask;
        }

        /// <summary>
        /// Stops service.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Successfully completed task.</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        #endregion

        #region Private Methods

        private void UpdatePlayerInfoProvider(object state)
        {
            var expirationDate = GetLastKeepingPlayersInfoDate();

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                IPlayerProviderCrudService playerProviderCrudService = scope.ServiceProvider.GetRequiredService<IPlayerProviderCrudService>();
                playerProviderCrudService.DeleteExpiredPlayerProvidersRangeAsync(expirationDate).Wait();               
            }
        }

        private DateTime GetLastKeepingPlayersInfoDate()
        {
            int keepingDeviceLogsDaysNumber = GetUpdateProviderPlayersInfoInDaysNumber();

            return DateTime.UtcNow.Add(-TimeSpan.FromDays(keepingDeviceLogsDaysNumber));
        }

        private int GetUpdateProviderPlayersInfoInDaysNumber()
        {
            string updateProviderPlayersInfoInDaysString = Environment.GetEnvironmentVariable("UpdateProviderPlayersInfoInDaysNumber");
            if (!int.TryParse(updateProviderPlayersInfoInDaysString, out int updateProviderPlayersInfoInDaysNumber))
            {
                _logger.LogCritical(
                    $"UpdateProviderPlayersInfoInDaysNumber {updateProviderPlayersInfoInDaysString} environment is not integer.");
                throw new InvalidOperationException(nameof(updateProviderPlayersInfoInDaysString));
            }

            return updateProviderPlayersInfoInDaysNumber;
        }

        #endregion
    }
}