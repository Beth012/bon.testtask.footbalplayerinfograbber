﻿using BON.FootbalPlayerInfoGrabber.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace BON.FootbalPlayerInfoGrabber.DAL.Context
{
    public class SportDBContext : DbContext
    {
        public SportDBContext(DbContextOptions<SportDBContext> options) : base(options)
        {
        }

        public DbSet<FootballPlayer> FootballPlayers { get; set; }

        public DbSet<PlayerProvider> PlayerProviders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<FootballPlayer>().Property(x => x.Name).IsRequired();
        }
    }
}
