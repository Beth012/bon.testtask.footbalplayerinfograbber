﻿namespace BON.FootbalPlayerInfoGrabber.DAL.Context
{
    public class BaseService
    {
        protected SportDBContext DbContext { get; set; }

        public BaseService(SportDBContext sportDBContext)
        {
            DbContext = sportDBContext;
        }
    }
}