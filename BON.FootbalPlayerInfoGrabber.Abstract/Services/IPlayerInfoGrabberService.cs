﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using System.Collections.Generic;

namespace BON.FootbalPlayerInfoGrabber.Abstract.Services
{
    /// <summary>
    /// Player info grabber interface.
    /// </summary>
    public interface IPlayerInfoGrabberService
    {
        /// <summary>
        /// Grabs player info from the specified provider.
        /// </summary>
        /// <param name="providerUrl">The provider url.</param>
        /// <returns>Grabbed player collection/</returns>
        IEnumerable<IPlayer> GrabPlayerInfo(string providerUrl);
    }
}
