﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using System;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.Abstract.Services
{
    /// <summary>
    /// Player provider data provider interface.
    /// </summary>
    public interface IPlayerProviderCrudService
    {
        /// <summary>
        /// Creates the player provider with specified url.
        /// </summary>
        /// <param name="playerProviderUrl">The provider url.</param>
        Task CreateAsync(string playerProviderUrl);

        /// <summary>
        /// Gets the specified player provider.
        /// </summary>
        /// <param name="providerUrl">The provider url.</param>
        /// <returns>The specified player provider.</returns>
        IPlayerProvider Get(string providerUrl);

        /// <summary>
        /// Deletes expired player provider collection.
        /// </summary>
        /// <param name="expirationDate">The expiration provider date.</param>
        Task DeleteExpiredPlayerProvidersRangeAsync(DateTime expirationDate);
    }
}
