﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.Abstract.Services
{
    /// <summary>
    /// Player data provider interface.
    /// </summary>
    public interface IPlayerCrudService
    {
        /// <summary>
        /// Creates the specified player collection.
        /// </summary>
        /// <param name="players">The player collection.</param>

        Task CreateRangeAsync(IEnumerable<IPlayer> players);

        /// <summary>
        /// Gets all players of the specified provider.
        /// </summary>
        /// <param name="providerUrl">The provider url.</param>
        /// <returns>All players of the provider.</returns>
        IEnumerable<IPlayer> Get(string providerUrl);
    }
}
