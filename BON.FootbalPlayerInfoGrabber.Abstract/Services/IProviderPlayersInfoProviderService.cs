﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.Abstract.Services
{
    /// <summary>
    /// Provider player info provider interface.
    /// </summary>
    public interface IProviderPlayersInfoProviderService
    {
        /// <summary>
        /// Gets all players of the specified provider.
        /// </summary>
        /// <param name="providerUrl">The provider url.</param>
        /// <returns>All players of the provider.</returns>
        Task<IEnumerable<IPlayer>> GetProviderPlayersAsync(string providerUrl);
    }
}
