﻿namespace BON.FootbalPlayerInfoGrabber.Abstract.Models
{
    /// <summary>
    /// Represents player data interface.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Gets or sets player provider data.
        /// </summary>
        IPlayerProvider PlayerProvider { get; set; }

        /// <summary>
        /// Gets or sets player image url.
        /// </summary>
        string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets player games number.
        /// </summary>
        int Games { get; set; }        
    }
}
