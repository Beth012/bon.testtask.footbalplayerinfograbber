﻿namespace BON.FootbalPlayerInfoGrabber.Abstract.Models
{
    /// <summary>
    /// Represents football player data interface.
    /// </summary>
    public interface IFootballPlayer : IPlayer
    {
        /// <summary>
        /// Gets or sets football player goals number.
        /// </summary>
        int Goals { get; set; }
    }
}
