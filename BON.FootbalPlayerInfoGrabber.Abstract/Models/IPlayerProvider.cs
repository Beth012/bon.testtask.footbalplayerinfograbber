﻿using System;

namespace BON.FootbalPlayerInfoGrabber.Abstract.Models
{
    /// <summary>
    /// Represents player provider data interface.
    /// </summary>
    public interface IPlayerProvider
    {
        /// <summary>
        /// Gets or sets player provider url.
        /// </summary>
        string ProviderUrl { get; set; }

        /// <summary>
        /// Gets or sets player provider creation date.
        /// </summary>
        DateTime CreationDate { get; set; }
    }
}
