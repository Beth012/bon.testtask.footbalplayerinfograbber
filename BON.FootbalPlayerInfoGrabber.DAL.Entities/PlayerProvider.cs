﻿using System;

namespace BON.FootbalPlayerInfoGrabber.DAL.Entities
{
    public class PlayerProvider
    {
        public int Id { get; set; }

        public string ProviderUrl { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
