﻿namespace BON.FootbalPlayerInfoGrabber.DAL.Entities
{
    public class FootballPlayer
    {
        public int Id { get; set; }

        public int PlayerProviderId { get; set; }

        public virtual PlayerProvider PlayerProvider { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public int Games { get; set; }

        public int Goals { get; set; }    
    }
}
