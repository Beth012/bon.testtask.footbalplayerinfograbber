﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.DAL.Entities;

namespace BON.FootbalPlayerInfoGrabber.Services.Abstract
{
    /// <summary>
    /// Player provider mapper interface.
    /// </summary>
    public interface IPlayerProviderMapper
    {
        /// <summary>
        /// Maps player provider to player provider entity model.
        /// </summary>
        /// <param name="playerProviderInstance">The player provider instance.</param>
        /// <returns>The player provider entity model.</returns>
        PlayerProvider ToPlayerProvider(IPlayerProvider playerProviderInstance);

        /// <summary>
        /// Maps player provider entity model to player provider instance.
        /// </summary>
        /// <param name="playerProvider">The player provider entity model.</param>
        /// <returns>The player provider instance.</returns>
        IPlayerProvider ToPlayerProviderModel(PlayerProvider playerProvider);
    }
}
