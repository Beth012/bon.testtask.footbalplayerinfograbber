﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Models;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Services
{
    /// <inheritdoc/>
    public class ExpressRuPlayerProvider : IPlayerInfoGrabberService
    {
        #region Fields

        private readonly IPlayerProviderCrudService _playerProviderCrud;
        private const string _playersLinesXPath = "(//table[contains(@class,'se19-table se19-table--td-center')]//tbody//tr)";
        private const string _playerImageUrlXPath = "//div[contains(@class, 'se19-player-card')]//div//img";
        private const string _playerNameXPath = "//h2[contains(@class, 'se19-player-subtitle mb_30')]";
        private const string _playerLinkXPathPart = "td//a";
        private const string _td = "td";
        private const string _div = "div";
        private const string _href = "href";
        private const string _exceptionalRole = "В";   
        private const string _src = "src";

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize new instance <see cref="ExpressRuPlayerProvider"/.>.
        /// </summary>
        /// <param name="playerProviderCrud">IPlayerProviderCrudService instance.</param>
        public ExpressRuPlayerProvider(IPlayerProviderCrudService playerProviderCrud)
        {
            _playerProviderCrud = playerProviderCrud;
        }

        #endregion

        #region IPlayerInfoGrabberService

        public IEnumerable<IPlayer> GrabPlayerInfo(string url)
        {
            var tableBody = GetTablePlayerLines(url).Result;

            return GetPlayerList(tableBody, url);
        }

        private IEnumerable<IPlayer> GetPlayerList(HtmlNodeCollection tableBody, string url)
        {
            List<Task<ExpressRuFootballPlayerModel>> listPlayers = new List<Task<ExpressRuFootballPlayerModel>>();

            foreach (HtmlNode tr in tableBody)
            {
                var tds = tr.SelectNodes(_td);

                if (IsNecessaryRole(tds[2]))
                {
                    listPlayers.Add(GetPlayerModel(tr, tds, url));
                }
            }
            
            Task.WhenAll(listPlayers);

            return listPlayers.Select(p => p.Result).ToList();
        }

        #endregion

        #region Private Methods

        private async Task<ExpressRuFootballPlayerModel> GetPlayerModel(HtmlNode tr, HtmlNodeCollection tds, string url)
        {
            ExpressRuFootballPlayerModel player = GrabPlayerListPageInfo(tds, url);

            await GrabPlayerPageInfo(tr, player);

            return player;
        }

        private ExpressRuFootballPlayerModel GrabPlayerListPageInfo(HtmlNodeCollection playerCells, string url)
        { 
            ExpressRuFootballPlayerModel player = new ExpressRuFootballPlayerModel 
            {
                PlayerProvider = _playerProviderCrud.Get(url)
            };

            int.TryParse(playerCells[3].InnerText, out int games);
            int.TryParse(playerCells[4].InnerText, out int goals);
            player.Games = games;
            player.Goals = goals;

            return player;

        }

        private async Task<HtmlNodeCollection> GetTablePlayerLines(string url)
        {
            var pageDocument = await GetHtmlDocuments(url);

            return pageDocument.SelectNodes(_playersLinesXPath);
        }

        private bool IsNecessaryRole(HtmlNode roleCell)
        {
            var role = roleCell.SelectSingleNode(_div).InnerText.Trim();

            return (role != _exceptionalRole);
        }

        private async Task GrabPlayerPageInfo(HtmlNode playerLine, ExpressRuFootballPlayerModel player)
        {
            var playerPageDocument = await GetPlayerPageDocument(playerLine);

            player.ImageUrl = playerPageDocument.SelectSingleNode(_playerImageUrlXPath).GetAttributeValue(_src, string.Empty);
            player.Name = playerPageDocument.SelectSingleNode(_playerNameXPath).InnerText;
        }

        private async Task<HtmlNode> GetPlayerPageDocument(HtmlNode playerLine)
        {
            string link = GetPlayerLink(playerLine);

            return await GetHtmlDocuments(link);
        }

        private string GetPlayerLink(HtmlNode playerLine)
        {
            return playerLine.SelectSingleNode(_playerLinkXPathPart).GetAttributeValue(_href, string.Empty);
        }

        private async Task<HtmlNode> GetHtmlDocuments(string url)
        {
            HttpClient httpClient = GetHttpClient();
            RegisterEbcondingProvider();
            var response = await GetResponse(httpClient, url);
            var pageDocument = await GetPageHtmlDocument(response);

            return pageDocument.DocumentNode;
        }

        private async Task<HtmlDocument> GetPageHtmlDocument(HttpResponseMessage response)
        {
            var pageContents = await response.Content.ReadAsStringAsync();

            return LoadPageHtmlDocument(pageContents);
        }

        private HtmlDocument LoadPageHtmlDocument(string pageContents)
        {
            HtmlDocument pageDocument = new HtmlDocument();
            pageDocument.LoadHtml(pageContents);

            return pageDocument;
        }

        private async Task<HttpResponseMessage> GetResponse(HttpClient httpClient, string url)
        {
            var response = await httpClient.GetAsync(url);

            while (response.StatusCode == HttpStatusCode.Found)
            {
                var location = response.Headers.Location;
                response = await httpClient.GetAsync(location);
            }

            return response;
        }

        private void RegisterEbcondingProvider()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private HttpClient GetHttpClient()
        {
            HttpClientHandler clientHandler = GetHttpClientHandler();

            return new HttpClient(clientHandler);
        }

        private HttpClientHandler GetHttpClientHandler()
        {
            return new HttpClientHandler
            {
                AllowAutoRedirect = true
            };
        }

        #endregion
    }
}
