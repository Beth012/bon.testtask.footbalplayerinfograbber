﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.Abstract.Services;
using BON.FootbalPlayerInfoGrabber.DAL.Context;
using BON.FootbalPlayerInfoGrabber.DAL.Entities;
using BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Models;
using BON.FootbalPlayerInfoGrabber.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Services
{
    /// <inheritdoc/>
    public class PlayerProvideService : BaseService, IPlayerProviderCrudService
    {
        #region Fields

        private readonly IPlayerProviderMapper _playerProviderMapper;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize new instance <see cref="PlayerProvideService"/.>.
        /// </summary>
        /// <param name="sportDBContext">SportDBContext instance.</param>
        /// <param name="playerProviderMapper">IPlayerProviderMapper instance.</param>
        public PlayerProvideService(SportDBContext sportDBContext, IPlayerProviderMapper playerProviderMapper) : base(sportDBContext)
        {
            _playerProviderMapper = playerProviderMapper;
        }

        #endregion

        #region IPlayerProviderCrudService

        public async Task CreateAsync(string playerProviderUrl)
        {
            if (string.IsNullOrWhiteSpace(playerProviderUrl))
                throw new InvalidOperationException(nameof(playerProviderUrl));

            var foundPlayerProvider = DbContext.PlayerProviders.FirstOrDefault(p => p.ProviderUrl == playerProviderUrl);

            if (foundPlayerProvider != null)
                throw new InvalidOperationException($"{nameof(foundPlayerProvider)} with url {playerProviderUrl} already exists");

            PlayerProviderModel playerProviderModel = new PlayerProviderModel
            {
                ProviderUrl = playerProviderUrl
            };

            await DbContext.PlayerProviders.AddAsync(_playerProviderMapper.ToPlayerProvider(playerProviderModel));
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteExpiredPlayerProvidersRangeAsync(DateTime expirationDate)
        {
            var expiredPlayrProviders = GetExpiredPlayerProviders(expirationDate);
            DbContext.PlayerProviders.RemoveRange(expiredPlayrProviders);
            await DbContext.SaveChangesAsync();
        }

        public IPlayerProvider Get(string providerUrl)
        {
            return DbContext.PlayerProviders.Where(p => p.ProviderUrl == providerUrl)
                .Select(_playerProviderMapper.ToPlayerProviderModel).FirstOrDefault();
        }

        #endregion

        #region Private Methods

        private IEnumerable<PlayerProvider> GetExpiredPlayerProviders(DateTime expirationDate)
        {
            return DbContext.PlayerProviders.Where(p => p.CreationDate <= expirationDate).ToList();
        }

        #endregion
    }
}
