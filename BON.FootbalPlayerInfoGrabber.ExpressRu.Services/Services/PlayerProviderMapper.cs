﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using BON.FootbalPlayerInfoGrabber.DAL.Entities;
using BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Models;
using BON.FootbalPlayerInfoGrabber.Services.Abstract;
using System;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Services
{
    /// <inheritdoc/>
    public class PlayerProviderMapper : IPlayerProviderMapper
    {
        #region IPlayerProviderMapper

        public PlayerProvider ToPlayerProvider(IPlayerProvider playerProviderInstance)
        {
            if (playerProviderInstance == null)
                throw new InvalidOperationException(nameof(playerProviderInstance));

            PlayerProvider playerProvider = new PlayerProvider
            {
                ProviderUrl = playerProviderInstance.ProviderUrl,
                CreationDate = DateTime.Now
            };

            if (playerProviderInstance is PlayerProviderModel playerProviderModel)
                playerProvider.Id = playerProviderModel.Id;

            return playerProvider;
        }

        public IPlayerProvider ToPlayerProviderModel(PlayerProvider playerProvider)
        {
            if (playerProvider == null)
                throw new InvalidOperationException(nameof(playerProvider));

            return new PlayerProviderModel
            {
                Id = playerProvider.Id,
                ProviderUrl = playerProvider.ProviderUrl,
                CreationDate = playerProvider.CreationDate
            };          
        }

        #endregion
    }
}
