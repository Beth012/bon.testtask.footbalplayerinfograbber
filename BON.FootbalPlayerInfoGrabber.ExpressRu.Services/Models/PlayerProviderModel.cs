﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;
using System;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Models
{
    /// <summary>
    ///  Represents player provider data.
    /// </summary>
    public class PlayerProviderModel : IPlayerProvider
    {
        /// <summary>
        /// Gets or sets player provider identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets player provider url.
        /// </summary>
        public string ProviderUrl { get; set; }

        /// <summary>
        /// Gets or sets player provider creation date.
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}