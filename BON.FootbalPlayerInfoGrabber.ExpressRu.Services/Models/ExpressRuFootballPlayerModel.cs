﻿using BON.FootbalPlayerInfoGrabber.Abstract.Models;

namespace BON.FootbalPlayerInfoGrabber.ExpressRu.Services.Models
{
    /// <summary>
    /// Represents expressRu football player data.
    /// </summary>
    public class ExpressRuFootballPlayerModel : IFootballPlayer
    {
        /// <summary>
        /// Gets or sets player provider data.
        /// </summary>
        public IPlayerProvider PlayerProvider { get; set; }

        /// <summary>
        /// Gets or sets player image url.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets player games number.
        /// </summary>
        public int Games { get; set; }

        /// <summary>
        /// Gets or sets football player goals number.
        /// </summary>
        public int Goals { get; set; }     
    }
}